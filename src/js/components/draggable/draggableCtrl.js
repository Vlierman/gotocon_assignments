(function () {
    'use strict';

    function DraggableCtrl($element) {
        var vm = this;

        /**
         * Tips!!! 
         * - On drag update position
         * - To update the position use $element.css
         */

        /**
         * Start dragging.
         * @param elCoordinates The current coordinates of the element.
         * Also init position: has to be the current position of the element
         */
        vm.start = function (elCoordinates) {
        };

        /**
         * Sets the offset coordinates.
         * @param offsetCoordinates The offset coordinates.
         */
        vm.setOffset = function(offsetCoordinates) {
        };

        /**
         * Drag. Set the css of the element
         */
        vm.drag = function () {
        };
    }

    DraggableCtrl.$inject = ['$element'];

    angular.module('awesome-directives').
        controller('adDraggableCtrl', DraggableCtrl);
})();