(function () {
    'use strict';

    function Draggable($window, $document, $parse) {
        return {
            restrict: 'A',
            controller: 'adDraggableCtrl',
            controllerAs: 'ctrl',
            link: function (scope, element, attrs, ctrl) {

                /**
                 * Tips!!! 
                 * - Bind mouse event to element
                 * - On mouse down event register mouse move and up
                 * - On mouse up even deregister mouse move and up 
                 * - For custom callback use $parse
                 */

                /**
                 * Handle start dragging.
                 * @param event The event.
                 */
                var down = function (event) {
                };

                /**
                 * Handle dragging.
                 * @param event The event.
                 */
                var move = function (event) {
                };

                /**
                 * Handle the stop dragging.
                 * @param event The event.
                 */
                var up = function (event) {
                };

                /**
                 * Attrs override:if we do the moving: make sure position is set to something ;-)
                 */
                if (angular.isUndefined(attrs.override)) {
                    if (element.css('position') === 'static' || element.css('position') == '') {
                        element.css({position: 'relative'});
                    }
                }
            }
        };
    }

    Draggable.$inject = ['$window', '$document', '$parse']

    angular.module('awesome-directives').
        directive('adDraggable', Draggable);
})();