'use strict';

var DraggablePO = require("./../po/DraggablePO");

describe('draggable', function() {

    beforeEach(function () {
        browser.get('/draggable/draggable.html');
    });

    it('should drag', function() {
        var draggable = new DraggablePO(element(by.id('draggable')));
        draggable.dragTo(100, 100);
        expect(draggable.hasMovedTo(100,100)).toBeTruthy();
    });

    it('should not drag', function() {
        var draggable = new DraggablePO(element(by.id('undraggable')));
        draggable.dragTo(100, 100);
        expect(draggable.hasMovedTo(100,100)).toBeFalsy();
    });

});