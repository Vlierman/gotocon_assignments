'use strict';

describe('Draggable', function () {
    var elm, scope, compile, controller, doc, win, preventDefault, parse, startCallback, moveCallback, stopCallback;

    beforeEach(function () {
        module('awesome-directives');
    });

    describe('when default', function () {
        beforeEach(function () {

            inject(function ($rootScope, $compile, $window, $document, $parse) {
                scope = $rootScope.$new();
                compile = $compile;
                doc = $document;
                win = $window;
                parse = $parse

                preventDefault = jasmine.createSpy();

                elm = angular.element(
                    '<div ad-draggable>draggable</div>'
                );

                compile(elm)(scope);
                scope.$digest();
            });
        });

        it('should bind mousemove and mouseup events on mousedown', function () {
            spyOn(doc, 'on');
            elm.triggerHandler({
                type: "mousedown",
                preventDefault: preventDefault
            });

            expect(doc.on).toHaveBeenCalledWith('mousemove', jasmine.any(Function));
            expect(doc.on).toHaveBeenCalledWith('mouseup', jasmine.any(Function));
            expect(preventDefault).toHaveBeenCalled();
        });

        it('should move', function () {
            elm.triggerHandler({
                type: "mousedown",
                preventDefault: preventDefault
            });

            doc.triggerHandler({
                type: "mousemove",
                preventDefault: preventDefault
            });
        });

        it('should unbind mousemove and mouseup events on mouseup', function () {
            spyOn(doc, 'off');
            elm.triggerHandler({
                type: "mousedown",
                preventDefault: preventDefault
            });

            doc.triggerHandler({
                type: "mouseup",
                preventDefault: preventDefault
            });

            expect(doc.off).toHaveBeenCalledWith('mousemove', jasmine.any(Function));
            expect(doc.off).toHaveBeenCalledWith('mouseup', jasmine.any(Function));
            expect(preventDefault).toHaveBeenCalled();
        });
    });

    describe('when override', function () {
        beforeEach(function () {

            inject(function ($rootScope, $compile, $window, $document, $parse) {
                scope = $rootScope.$new();
                compile = $compile;
                doc = $document;
                win = $window;
                parse = $parse

                preventDefault = jasmine.createSpy();
                startCallback = jasmine.createSpy();
                moveCallback = jasmine.createSpy();
                stopCallback = jasmine.createSpy();

                elm = angular.element(
                    '<div ad-draggable on-drag-start="start" on-drag="move" on-drag-end="stop" override="true">draggable</div>'
                );

                scope.start = startCallback;
                scope.move = moveCallback;
                scope.stop = stopCallback;

                compile(elm)(scope);
                scope.$digest();
            });
        });

        it('should bind mousemove and mouseup events on mousedown', function () {
            spyOn(doc, 'on');
            elm.triggerHandler({
                type: "mousedown",
                preventDefault: preventDefault
            });

            expect(doc.on).toHaveBeenCalledWith('mousemove', jasmine.any(Function));
            expect(doc.on).toHaveBeenCalledWith('mouseup', jasmine.any(Function));
            expect(preventDefault).toHaveBeenCalled();
            expect(startCallback).toHaveBeenCalled();
        });

        it('should move', function () {
            elm.triggerHandler({
                type: "mousedown",
                preventDefault: preventDefault
            });

            doc.triggerHandler({
                type: "mousemove",
                preventDefault: preventDefault
            });
            expect(moveCallback).toHaveBeenCalled();
        });

        it('should unbind mousemove and mouseup events on mouseup', function () {
            spyOn(doc, 'off');
            elm.triggerHandler({
                type: "mousedown",
                preventDefault: preventDefault
            });

            doc.triggerHandler({
                type: "mouseup",
                preventDefault: preventDefault
            });

            expect(doc.off).toHaveBeenCalledWith('mousemove', jasmine.any(Function));
            expect(doc.off).toHaveBeenCalledWith('mouseup', jasmine.any(Function));
            expect(preventDefault).toHaveBeenCalled();
            expect(stopCallback).toHaveBeenCalled();
        });
    });

    describe('when on every browser', function () {
        beforeEach(function () {

            inject(function ($rootScope, $compile, $window, $document, $parse) {
                scope = $rootScope.$new();
                compile = $compile;
                doc = $document;
                win = $window;
                parse = $parse;
            });
        });

        it('should set position:relative when current position is static', function () {
            elm = angular.element(
                '<div ad-draggable style="position:static">draggable</div>'
            );

            compile(elm)(scope);
            scope.$digest();

            expect(elm.css('position')).toBe('relative');
        });

        it('should set position:relative when current position is unset', function () {
            elm = angular.element(
                '<div ad-draggable>draggable</div>'
            );

            compile(elm)(scope);
            scope.$digest();

            expect(elm.css('position')).toBe('relative');
        });

        it('should not set position:relative when current position is absolute', function () {
            elm = angular.element(
                '<div ad-draggable style="position:absolute">draggable</div>'
            );

            compile(elm)(scope);
            scope.$digest();

            expect(elm.css('position')).toBe('absolute');
        });

        it('should not set position:relative when current position is fixed', function () {
            elm = angular.element(
                '<div ad-draggable style="position:fixed">draggable</div>'
            );

            compile(elm)(scope);
            scope.$digest();

            expect(elm.css('position')).toBe('fixed');
        });

        it('should not set position:relative when current position is inherit', function () {
            elm = angular.element(
                '<div ad-draggable style="position:inherit">draggable</div>'
            );

            compile(elm)(scope);
            scope.$digest();

            expect(elm.css('position')).toBe('inherit');
        });
    });
});