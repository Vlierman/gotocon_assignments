'use strict';

describe('DraggableCtrl', function () {
    var $controller;

    beforeEach(function () {
        module('awesome-directives');

        inject(function (_$controller_) {
            $controller = _$controller_;
        });
    });


    it('should set positions to 0 when no top and left have been set', function () {
        var controller = $controller('adDraggableCtrl', {
            $element: angular.element('<div></div>')
        });

        controller.start({x:0,y:0});

        expect(controller.coordinates).toEqual({x:0, y:0});
        expect(controller.position).toEqual({top:0, left:0});
    });

    it('should set position top to 0 when no top has been set', function () {
        var controller = $controller('adDraggableCtrl', {
            $element: angular.element('<div style="left:1px;"></div>')
        });

        controller.start({x:0,y:0});

        expect(controller.coordinates).toEqual({x:0, y:0});
        expect(controller.position).toEqual({top:0, left:1});
    });

    it('should set position left to 0 when no left has been set', function () {
        var controller = $controller('adDraggableCtrl', {
            $element: angular.element('<div style="top:1px;"></div>')
        });

        controller.start({x:0,y:0});

        expect(controller.coordinates).toEqual({x:0, y:0});
        expect(controller.position).toEqual({top:1, left:0});
    });

    it('should set offset', function () {
        var controller = $controller('adDraggableCtrl', {
            $element: angular.element('<div></div>')
        });

        controller.start({x:100,y:100});
        expect(controller.coordinates).toEqual({x:100, y:100});
        controller.setOffset({x:120,y:120});

        expect(controller.offset).toEqual({x:20, y:20});
    });

    it('should drag', function () {
        var el = angular.element('<div></div>');
        var controller = $controller('adDraggableCtrl', {
            $element: el
        });

        controller.start({x:100,y:100});
        expect(controller.coordinates).toEqual({x:100, y:100});
        controller.setOffset({x:120,y:120});

        expect(el.css('top')).toBe('');
        expect(el.css('left')).toBe('');

        controller.drag();
        expect(el.css('top')).toBe('20px');
        expect(el.css('left')).toBe('20px');
    });
});
