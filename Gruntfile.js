'use strict';

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        config: {
            hosts: {
                runtime: 'localhost'
            },
            paths: {
                tmp: '.tmp',
                staging: '.staging',
                src: 'src',
                test: 'test',
                results: 'results',
                instrumented: 'instrumented',
                config: 'config',
                dist: 'dist'
            }
        },
        clean: {
            files: [
                '<%= config.paths.tmp %>',
                '<%= config.paths.staging %>',
                '<%= config.paths.results %>',
                '<%= config.paths.instrumented %>',
                '<%= config.paths.dist %>'
            ]
        },
        portPick: {
            options: {
                port: 9000
            },
            connect: {
                targets: [
                    'connect.options.port'
                ]
            },
            protractor: {
                targets: [
                    'protractor_coverage.options.collectorPort'
                ]
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.paths.src %>',
                        dest: '<%= config.paths.dist %>',
                        src: 'index.html'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.paths.src %>',
                        dest: '<%= config.paths.dist %>',
                        src: 'img/{,*/}*.{gif,webp,svg,png,jpg}'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.paths.src %>',
                        dest: '<%= config.paths.dist %>',
                        src: 'css/{,*/}*.css.map'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'node_modules/bootstrap',
                        dest: '<%= config.paths.dist %>',
                        src: 'fonts/{,*/}*.{eot,svg,ttf,woff,woff2}'
                    }
                ]
            }
        },
        jshint: {
            options: {
                jshintrc: '<%= config.paths.config %>/.jshintrc',
                reporter: require('jshint-junit-reporter'),
                reporterOutput: '<%= config.paths.results %>/jshint/jshint.xml'
            },
            files: {
                src: ['<%= config.paths.src %>/**/*.js']
            }
        },
        karma: {
            options: {
                singleRun: true,
                reporters: ['progress', 'coverage', 'junit']
            },
            unit: {
                configFile: '<%= config.paths.config %>/karma.conf.js'
            }
        },
        instrument: {
            files: '<%= config.paths.src %>/**/*.js',
            options: {
                basePath: '<%= config.paths.instrumented %>',
                lazy: false
            }
        },
        connect: {
            options: {
                port: 0,
                hostname: '0.0.0.0'
            },
            test: {
                options: {
                    open: false,
                    middleware: function (connect) {
                        var config = grunt.config.get('config');
                        return [
                            connect().use('/node_modules', connect.static('./node_modules')),
                            connect().use('/js', connect.static(config.paths.instrumented + '/' + config.paths.src + '/js')),
                            connect().use('/', connect.static(config.paths.src)),
                            connect().use('/', connect.static(config.paths.test + '/protractor')),
                        ];
                    }
                }
            }, 
            runtime: {
                options: {
                    open: true,
                    port:9000,
                    middleware: function (connect) {
                        var config = grunt.config.get('config');
                        return [
                            connect().use('/node_modules', connect.static('./node_modules')),
                            connect().use('/js', connect.static(config.paths.src + '/js')),
                            connect().use('/test', connect.static('./test')),
                            connect().use('/', connect.static(config.paths.src)),

                        ];
                    }
                }
            }
        },
        watch: {
            test: {
                files: ['<%=config.paths.instrumented%>/<%=config.paths.src%>/**/*.js']
            },
            runtime: {
                files: ['<%=config.paths.src%>/**/*.js']
            }
        },
        protractor_coverage: {
            options: {
                configFile: 'config/protractor-local.conf.js',
                keepAlive: true,
                noColor: false,
                debug: false,
                collectorPort: 0
            },
            all: {
                options: {
                    coverageDir: '<%= config.paths.results %>/protractor-coverage',
                    args: {
                        baseUrl: 'http://<%= config.hosts.runtime %>:<%= connect.test.options.port %>',
                        specs: ['<%= config.paths.test %>/protractor/**/*Spec.js'],
                        params: {
                            resultsDir: '<%= config.paths.results %>/protractor'
                        }
                    }
                }
            }
        },
        makeReport: {
            src: '<%= config.paths.results %>/protractor-coverage/*.json',
            options: {
                type: 'lcov',
                dir: '<%= config.paths.results %>/protractor-coverage',
                print: 'detail'
            }
        },
        htmlmin: {
            partials: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.paths.src %>/partials',
                        src: ['**/*.html'],
                        dest: '<%= config.paths.tmp %>/partials'
                    }
                ]
            },
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.paths.dist %>',
                        src: ['**/*.html'],
                        dest: '<%= config.paths.dist %>'
                    }
                ]
            }
        },
        ngtemplates: {
            generated: {
                options: {
                    module: 'awesome-directives'
                },
                cwd: '<%= config.paths.tmp %>',
                src: 'partials/{,*/}*.html',
                dest: '<%= config.paths.tmp %>/js/templates.js'
            }
        },
        concat: {
            options: {
                separator: grunt.util.linefeed + ';' + grunt.util.linefeed

            },
            all: {
                files: [
                    {
                        dest: '<%= config.paths.dist %>/awesome-directives.js',
                        src: [
                            '<%= config.paths.src %>/js/awesome-directives.js',
                            '<%= config.paths.src %>/js/**/*.js',
                            '<%= config.paths.tmp %>/js/templates.js'
                        ]
                    }
                ]
            }

        },
        ngAnnotate: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= config.paths.tmp %>/scripts',
                        src: '**/*.js',
                        dest: '<%= config.paths.tmp %>/scripts'
                    }
                ]
            }
        },
        uglify: {
            options: {
                banner: '/*! awesome-directives <%= grunt.template.today("dd-mm-yyyy HH:MM:ss") %> */\n'
            },
            all: {
                files: {
                    '<%= config.paths.dist%>/awesome-directives.min.js': ['<%=config.paths.dist %>/awesome-directives.js']
                }
            }
        }
    });

    grunt.registerTask('prepare', 'Prepare all the necessary stuff.', [
        'clean',
        'portPick',
        'copy'
    ]);

    grunt.registerTask('test', 'Execute tests.', [
        'force:on',
        'jshint',
        'karma',
        'instrument',
        'connect:test',
        'protractor_coverage',
        'makeReport',
        'force:reset'
    ]);

    grunt.registerTask('package', 'Package the code in a distributable format.', [
        'htmlmin:partials',
        'ngtemplates',
        'concat',
        'ngAnnotate',
        'uglify'
    ]);

    grunt.registerTask('default', 'Default task', [
        'prepare',
        'test',
        'package'
    ]);

    grunt.registerTask('serve', 'Default task', [
        'connect:runtime',
        'watch'
    ]);
};